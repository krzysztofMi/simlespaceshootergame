#include "bullet.h"

Bullet::Bullet(float x, float y):velocity{-50}{
    load("Images/bullet.png");
    getSprite().setPosition(x, y);
    getSprite().setOrigin(getSprite().getGlobalBounds().width/2,
                          getSprite().getGlobalBounds().height/2);
}

void Bullet::update(sf::Time time){
    getSprite().move(0, velocity * time.asSeconds());
}
