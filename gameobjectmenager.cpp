#include "gameobjectmenager.h"

GameObjectMenager* GameObjectMenager::instance = nullptr;

GameObjectMenager* GameObjectMenager::getInstance(){
    if(instance != nullptr){
        return instance;
    }
    instance = new GameObjectMenager{};
    return instance;
}

void GameObjectMenager::add(const std::string& name, const std::shared_ptr<GameObject>& object){
    gameObjects.insert(std::pair<std::string, std::shared_ptr<GameObject>>(name, object));
}

void GameObjectMenager::remove(const std::string& name){
    if(gameObjects.find(name)!=gameObjects.end()){
        gameObjects.erase(name);
    }
}

std::shared_ptr<GameObject> GameObjectMenager::getGameObject(const std::string& name) const{
    if(gameObjects.find(name)!=gameObjects.end()){
        return gameObjects.at(name);
    }
    return nullptr;
}

void GameObjectMenager::drawAll(sf::RenderWindow &window){
    for(auto elem: gameObjects){
        elem.second->draw(window);
    }
}

void GameObjectMenager::updateAll(){
    sf::Time time = clock.restart();
    for(auto elem: gameObjects){
        elem.second->update(time);
    }
}
