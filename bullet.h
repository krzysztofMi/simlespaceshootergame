#ifndef BULLET_H
#define BULLET_H
#include "gameobject.h"

class Bullet:public GameObject{
    float velocity;
public:
    Bullet(float, float);
    virtual void update(sf::Time);
};

#endif // BULLET_H
