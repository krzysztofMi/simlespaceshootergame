#ifndef GAMEOBJECTMENAGER_H
#define GAMEOBJECTMENAGER_H
#include <map>
#include <memory>
#include "gameobject.h"
class GameObjectMenager
{
    static GameObjectMenager* instance;
    GameObjectMenager() = default;
    std::map<std::string, std::shared_ptr<GameObject>> gameObjects;
    sf::Clock clock;
public:
    static GameObjectMenager* getInstance();
    void add(const std::string&, const std::shared_ptr<GameObject>&);
    void remove(const std::string&);
    std::shared_ptr<GameObject> getGameObject(const std::string&) const;

    void drawAll(sf::RenderWindow &window);
    void updateAll();
};

#endif // GAMEOBJECTMENAGER_H
