#include "enemyship.h"
#include "game.h"

int EnemyShip::enemyCount = 0;

EnemyShip::EnemyShip(float x, float y, float velocity):
                                       velocityX{velocity}{
    velocityY = velocityX;
    if(velocityX<0){
        velocityY = -velocityX;
    }
    step = 0;
    enemyCount ++;
    load("Images/enemyShip.png");
    getSprite().setPosition(x, y);
    getSprite().setOrigin(getSprite().getGlobalBounds().width/2,
                          getSprite().getGlobalBounds().height/2);
}

void EnemyShip::update(sf::Time time){
    enemyMove(time);
}


void EnemyShip::enemyMove(sf::Time time){
    sf::Vector2f pos = this->getPosition();
    if((pos.x < getSprite().getGlobalBounds().width/2
        || pos.x > Game::windowWidth + getSprite().getGlobalBounds().width+120)
        && (step < 2000)){
        if(step ==1999)
            velocityX=-velocityX;
        getSprite().move(0, velocityY * time.asSeconds());
        step++;
    }
    else{
        if(step>=2000){
            step++;
            if(step>2005){
                step = 0;
            }
        }
        getSprite().move(velocityX * time.asSeconds(), 0);
    }
}
