#include "game.h"
#include "enemyship.h"

Game* Game::instance = nullptr;
sf::RenderWindow Game::window;
const int Game::windowHight = 1920;
const int Game::windowWidth = 1080;
sf::Event Game::event;

Game* Game::getInstance(){
    if(instance != nullptr){
        return instance;
    }
    instance = new Game();
    return instance;
}

Game::Game():gameState{GameState::UNINITIALIZED}{
    window.create(sf::VideoMode(windowHight, windowWidth, 32), "Buum!");
    std::shared_ptr<GameObject> ship{new OwnShip(600, windowWidth/2 + 100)};
    gameObjectMenager = GameObjectMenager::getInstance();
    float x1 = 50;
    float y1 = 100;
    float x2 = 1250;
    float y2 = 245;
    float velocity1 = 50;
    float velocity2 = -50;
    for(int i = 0; i<10; i++){
        std::shared_ptr<GameObject> enemyShip1{
            new EnemyShip{x1, y1, velocity1}};
        std::shared_ptr<GameObject> enemyShip2{
            new EnemyShip{x2, y2, velocity2}};
        gameObjectMenager->add("enemyShip1"+i, enemyShip1);
        gameObjectMenager->add("enemyShip2"+i, enemyShip2);
        x1+=100;
        x2-=100;
    }
    gameObjectMenager->add("ownShip", ship);
}

void Game::playEventLoop(){
    while (window.pollEvent(event)) {
        if(event.type == sf::Event::Closed){
            gameState = GameState::EXIT;
        }
        if(event.type == sf::Event::KeyPressed){
            if(event.key.code == sf::Keyboard::Escape){
                gameState = GameState::EXIT;
            }
        }
    }
}

void Game::startGame(){
    if(gameState != GameState::UNINITIALIZED){
        return;
    }
    gameState = GameState::PLAYING;
    while(!isClosed()){
        gameLoop();
    }
    window.close();
}

bool Game::isClosed(){
    if(gameState == GameState::EXIT){
        return true;
    }
    return false;
}

void Game::gameLoop(){
    switch(gameState){
        case GameState::PLAYING:
            playEventLoop();
            window.clear();
            gameObjectMenager->updateAll();
            gameObjectMenager->drawAll(window);

            window.display();
            break;
        case GameState::EXIT:
            break;
        case GameState::UNINITIALIZED:
            break;
    }
}

const sf::Event& Game::getInput(){
    window.pollEvent(event);
    return event;
}
