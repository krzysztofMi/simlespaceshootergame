#ifndef GAMEOBJECT_H
#define GAMEOBJECT_H
#include <SFML/Graphics.hpp>
#include <string>


class GameObject
{

    sf::Sprite sprite;
    sf::Image image;
    sf::Texture texture;
    std::string path;
    bool loaded;
protected:
    sf::Sprite& getSprite();
    void isLoaded(bool);
public:
    GameObject();
    ~GameObject();
    void load(const std::string);
    void setPosition(float, float);
    sf::Vector2f getPosition() const;
    bool isLoaded() const;
    void draw(sf::RenderWindow&);
    virtual void update(sf::Time) = 0;
};

#endif // GAMEOBJECT_H
