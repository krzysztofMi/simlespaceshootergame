#include "ownship.h"
#include "game.h"
#include "enemyship.h"

OwnShip::OwnShip(float x, float y): velocity{0},
                    maxVelocity{600}{
    load("Images/ship.png");
    getSprite().setPosition(x, y);
    getSprite().setOrigin(getSprite().getGlobalBounds().width/2,
                          getSprite().getGlobalBounds().height/2);
}


void OwnShip::update(sf::Time time){
    sf::Event event = Game::getInput();
    if(event.type == sf::Event::KeyPressed){
        switch(event.key.code){
            case sf::Keyboard::Right:{
                velocity += 3.0f;
                break;
            }
            case sf::Keyboard::Left:{
                velocity -= 3.0f;
                break;
            }
            case sf::Keyboard::Down:{
                velocity = 0;
                break;
            }
            case sf::Keyboard::Up:{
                shot();
                break;
            }
            default:
                break;
        }
        if(velocity>maxVelocity){
            velocity = maxVelocity;
        }
        if(velocity<-maxVelocity){
            velocity=-maxVelocity;
        }
        sf::Vector2f pos = this->getPosition();
        if(pos.x < getSprite().getGlobalBounds().width/2
            || pos.x > Game::windowWidth + getSprite().getGlobalBounds().width+120){
            velocity = - velocity;
        }
        getSprite().move(velocity * time.asSeconds(), 0);

        if(!wasPressedUP){
            pressedCount++;
            if(pressedCount == 1000){
                pressedCount = 0;
                wasPressedUP = true;
            }
        }
    }
    for(int i = 0; i<bulletTab.size(); i++){
        if(bulletTab.at(i)->getPosition().y>Game::windowHight){

        }
    }
}

void OwnShip::shot(){
    velocity = 0;
    if(wasPressedUP){
        std::shared_ptr<Bullet> bullet{new Bullet{getSprite().getPosition().x,
                                                 getSprite().getPosition().y}};
        bulletTab.push_back(bullet);
        GameObjectMenager *game = GameObjectMenager::getInstance();
        game->add(std::to_string(bulletTab.size())+"bull", bullet);
        wasPressedUP = false;
    }
}

std::vector<std::shared_ptr<Bullet>>& OwnShip::getBullets(){
    return bulletTab;
}
