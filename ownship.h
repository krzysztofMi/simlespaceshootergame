#ifndef OWNSHIP_H
#define OWNSHIP_H

#include "gameobject.h"
#include "bullet.h"
#include <vector>
#include <memory>

class OwnShip: public GameObject{
    bool wasPressedUP = true;
    int pressedCount = 0;
    float velocity;
    float maxVelocity;
    std::vector<std::shared_ptr<Bullet>> bulletTab;
    void shot();
public:
    OwnShip(float, float);
    std::vector<std::shared_ptr<Bullet>>& getBullets();
   ~OwnShip() = default;
    virtual void update(sf::Time);
    float getVelocity() const;
};

#endif // OWNSHIP_H
