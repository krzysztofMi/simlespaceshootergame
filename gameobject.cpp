#include "gameobject.h"

GameObject::GameObject(): loaded{false} {}

GameObject::~GameObject(){

}

void GameObject::isLoaded(bool isOrNot){
    loaded = isOrNot;
}

void GameObject::load(std::string path){
    if(!texture.loadFromFile(path)){
        path = "";
        loaded = false;
        return;
    }
    this->path = path;
    sprite.setTexture(texture);
    loaded = true;
}

void GameObject::draw(sf::RenderWindow & window){
    if(loaded){
        window.draw(sprite);
    }
}

void GameObject::setPosition(float x, float y){
    if(loaded){
        sprite.setPosition(x, y);
    }
}



sf::Vector2f GameObject::getPosition() const{
    if(loaded){
        return sprite.getPosition();
    }
    return sf::Vector2f{};
}

bool GameObject::isLoaded() const{
    return loaded;
}

sf::Sprite& GameObject::getSprite(){
    return sprite;
}
