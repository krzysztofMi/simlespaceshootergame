#ifndef GAME_H
#define GAME_H

#include <SFML/System.hpp>
#include <SFML/Graphics.hpp>
#include "ownship.h"
#include "gameobjectmenager.h"

enum class GameState{
    UNINITIALIZED,
    PLAYING,
    EXIT
};


class Game{
    static Game *instance;
    static sf::RenderWindow window;
    GameState gameState;
    static sf::Event event;
    GameObjectMenager *gameObjectMenager;
    bool isClosed();
    void gameLoop();
    void playEventLoop();
    Game();
public: 
    const static int windowHight;
    const static int windowWidth;

    static Game* getInstance();
    void startGame();
    static const sf::Event& getInput();
};

#endif // GAME_H
