#ifndef ENEMYSHIP_H
#define ENEMYSHIP_H

#include "gameobject.h"

class EnemyShip: public GameObject{
    static int enemyCount;
    int step;
    float velocityX;
    float velocityY;
    void enemyMove(sf::Time);
public:
    EnemyShip(float, float, float);
    virtual void update(sf::Time);
};

#endif // ENEMYSHIP_H
